function Connect-NSXTManager {

	<#
	.SYNOPSIS
	Connects to NSX-T Manager
	
	.DESCRIPTION
	As REST API is not connection-oriented, we can't actually connect to server and keep connection.
	Instead, are trying to create some proxy connection object to use in following calls
	
	.PARAMETER Server
	The FQDN or IP of your NSX Manager
	
	.PARAMETER Username
	The username to connect with. Defaults to admin if nothing is provided.
	
	.PARAMETER Password
	The password to connect with
	
	.PARAMETER DefaultConnection
	Will be used as default connection for API calls to follow, if set to true. Default is True
	
	.EXAMPLE
	PS> Connect-NSXTManager -Server "nsxmgr.corp.local" -Username admin -Password passwd123
	#>
	
	[CmdletBinding()] 
	param (
		[Parameter(Mandatory=$true,Position=0)]
			[String]$Server,
		[Parameter(Mandatory=$false,Position=1)]
			[String]$Username = "admin",
		[Parameter(Mandatory=$true)]
			[String]$Password,
		[Parameter(Mandatory=$false)]
			[Bool]$DefaultConnection=$true
	)
	
	process {
        $URI = "/api/v1/global-configs"
		$cred = New-Object System.Management.Automation.PSCredential($Username, $(ConvertTo-SecureString $Password -AsPlainText -Force))
		try {
			$response = Invoke-NSXRestAPICall -Cred $cred -Server $Server -URI $URI -Method "get"
		}
		catch {
			Throw "Unable to connect NSX Manager $NSXManager. $_"
		}
		$connection = New-Object PSCustomObject
		$connection | Add-Member -MemberType NoteProperty -Force -Name "Server" -Value $Server
		$connection | Add-Member -MemberType NoteProperty -Force -Name "Credential" -Value $cred
		
		if ($DefaultConnection) {
			Set-Variable -Name DefaultNSXConnection -Value $connection -Scope Global
		}
		$connection
	}
}
function Invoke-NSXRestAPICall {

	#Internal method to construct the REST call headers including auth as expected by NSX.
    #Accepts either a connection object as produced by Connect-NSXServer or explicit
    #parameters.
	#Ripped from PowerCLI module, simplified and adapted for NSX-T
	
	[CmdletBinding(DefaultParameterSetName="ConnectionSet")]
	param (
		[Parameter(Mandatory=$true,ParameterSetName="ManualSet")]
			[System.Management.Automation.PSCredential]$Cred,
		[Parameter(Mandatory=$true,ParameterSetName="ManualSet")]
			[String]$Server,
		[Parameter(Mandatory=$true,ParameterSetName="ManualSet")]
		[Parameter(ParameterSetName="ConnectionSet")]
			[String]$Method,
		[Parameter(Mandatory=$true,ParameterSetName="ManualSet")]
		[Parameter(ParameterSetName="ConnectionSet")]
			[String]$URI,
		[Parameter(Mandatory=$false,ParameterSetName="ManualSet")]
		[Parameter(ParameterSetName="ConnectionSet")]
			[String]$JSONBody,
		[Parameter(Mandatory=$false,ParameterSetName="ManualSet")]
		[Parameter(ParameterSetName="ConnectionSet")]
			[bool]$SkipCertificateCheck=$true,
		[Parameter(Mandatory=$false,ParameterSetName="ManualSet")]
		[Parameter(ParameterSetName="ConnectionSet")]
			[bool]$DisableKeepAlive=$false,
		[Parameter(Mandatory=$false,ParameterSetName="ConnectionSet")]
			[psObject]$Connection
	)
	
	process {
		if ($PSCmdlet.ParameterSetName -eq "ConnectionSet") {
			if ($Connection -eq $null) {
				#Now we need to assume that defaultnsxconnection does not exist...
	            if ( -not (test-path variable:global:DefaultNSXConnection) ) { 
	                throw "Not connected.  Connect to NSX manager with Connect-NsxServer first." 
	            }
	            else { 
	                Write-Debug "$($MyInvocation.MyCommand.Name) : Using default connection"
	                $Connection = $DefaultNSXConnection
	            }   
			}
			
			$Cred = $Connection.Credential
			$Server = $Connection.Server
		}
		
		$base64cred = [System.Convert]::ToBase64String(
			[System.Text.Encoding]::UTF8.GetBytes(
				$Cred.GetNetworkCredential().UserName + ":" + $Cred.GetNetworkCredential().password
			)
		)
		$headerDictionary = @{"Authorization"="Basic $base64cred"}
						
		$request = "https://$($Server)$($URI)"  # equals "https://" + $Server + $URI
		
		try {
			if ($Method -eq "patch") {
				$response = Invoke-RestMethod -Uri $request -Headers $headerDictionary -ContentType "application/json" -Method $Method -Body $JSONBody -DisableKeepAlive:$DisableKeepAlive -SkipCertificateCheck:$SkipCertificateCheck
			}
			else {
				$response = Invoke-RestMethod -Uri $request -Headers $headerDictionary -ContentType "application/json" -Method $Method -DisableKeepAlive:$DisableKeepAlive -SkipCertificateCheck:$SkipCertificateCheck
			}
		}
		catch {
            $response = $_.Exception.Response
			$statusCode = $_.Exception.Response.StatusCode.value__
			$messageObj = New-Object PSCustomObject
			$messageObj = $_.ErrorDetails.Message | ConvertFrom-Json
			$errorMessage = $messageObj.error_message
			$errorString = "Invoke-NSXRestAPICall: Status code: $($statusCode). Error message: $($errorMessage) Method: $($Method) URI: $($URI)"
			foreach ($err in $messageObj.related_errors) {
				$errorString += "`n"
				$errorString += $err.error_message
			}
            throw $errorString
            # $errorMessage = ($_.ErrorDetails.Message | ConvertFrom-Json).error_message
            # $errorString = "Invoke-NSXRestAPICall: Status code: $($statusCode). Error message: $($errorMessage) Method: $($Method) URI: $($URI)"
            # throw $errorString
        }
		$response
	}
}

function Get-ManagementAPIObjects {
	param (
		[Parameter(Mandatory=$true)]
			[string]$URI,
		[Parameter(Mandatory=$false)]
			[switch]$FilterOutPolicy
		
	)
	process {
		$result = @()
		$cursor = ""
		$temp = Invoke-NSXRestAPICall -Connection $NSXConnection -URI $URI -Method "get"
		$result += $temp.results
		$x = [math]::Floor($temp.result_count / 1000) - 1
		for ($i=0; $i -lt $x; $i++) {
			$cursor = "&cursor=$($temp.cursor)"
			$cursorURI = "$($URI)$($cursor)"
			$temp = Invoke-NSXRestAPICall -Connection $NSXConnection -URI $cursorURI -Method "get"
			$result += $temp.results
		}
		if ($FilterOutPolicy) {
			$result | Where-Object { $_._create_user -ne "nsx_policy" }
		}
		else {
			$result
		}
	}
}

# 
# 
# 
# 

function New-Monitors {
	param (
		[Parameter(Mandatory=$true)]
			[PSObject]$InObject,
		[Parameter(Mandatory=$false)]
			[switch]$DeleteFlag
	)
	process {
		$result = @()
		foreach ($Mon in $InObject) {
			if ((($Mon._create_user -eq "system") -or ($Mon._create_user -eq "admin")) -and ($Mon._protection -eq "NOT_PROTECTED")) {
#				$MonitorsHash.add($Mon.id,"/infra/lb-monitor-profiles/$($Mon.display_name.Replace(" ","_"))")
#				$MonitorsHash.add($Mon.id,"/infra/lb-monitor-profiles/$($Mon.id)")
				### init wrapper object
				$resObj = New-Object PSCustomObject
				$resObj | Add-Member -NotePropertyName "resource_type" -NotePropertyValue "ChildLBMonitorProfile"
				if ($DeleteFlag) {
					$resObj | Add-Member -NotePropertyName "marked_for_delete" -NotePropertyValue $true
				}
				### init data object
				$LBMonitorProfile = New-Object PSCustomObject
				## set generic parameters
				if ($DeleteFlag) {
					$LBMonitorProfile | Add-Member -NotePropertyName "marked_for_delete" -NotePropertyValue $true
				}

				if ($Mon.description) {
					$LBMonitorProfile | Add-Member -NotePropertyName "description" -NotePropertyValue $Mon.description
				}
				$LBMonitorProfile | Add-Member -NotePropertyName "display_name" -NotePropertyValue $Mon.display_name
				$LBMonitorProfile | Add-Member -NotePropertyName "id" -NotePropertyValue $Mon.id 
				if ($Mon.tags) {
					$LBMonitorProfile | Add-Member -NotePropertyName "tags" -NotePropertyValue $Mon.tags
				}
				$LBMonitorProfile | Add-Member -NotePropertyName "timeout" -NotePropertyValue $Mon.timeout
				## monitor-specific parameters
				switch ($Mon.resource_type) {
					"LbHttpMonitor" {
						$LBMonitorProfile | Add-Member -NotePropertyName "resource_type" -NotePropertyValue "LBHttpMonitorProfile"
						$LBMonitorProfile | Add-Member -NotePropertyName "fall_count" -NotePropertyValue $Mon.fall_count
						$LBMonitorProfile | Add-Member -NotePropertyName "interval" -NotePropertyValue $Mon.interval 
						if ($Mon.monitor_port) {
							$LBMonitorProfile | Add-Member -NotePropertyName "monitor_port" -NotePropertyValue $Mon.monitor_port
						}
						$LBMonitorProfile | Add-Member -NotePropertyName "rise_count" -NotePropertyValue $Mon.rise_count
						if ($Mon.request_body) {
							$LBMonitorProfile | Add-Member -NotePropertyName "request_body" -NotePropertyValue $Mon.request_body
						}
						if ($Mon.request_headers) {
							$LBMonitorProfile | Add-Member -NotePropertyName "request_headers" -NotePropertyValue $Mon.request_headers
						}
						$LBMonitorProfile | Add-Member -NotePropertyName "request_method" -NotePropertyValue $Mon.request_method
						if ($Mon.request_url) {
							$LBMonitorProfile | Add-Member -NotePropertyName "request_url" -NotePropertyValue $Mon.request_url
						}
						$LBMonitorProfile | Add-Member -NotePropertyName "request_version" -NotePropertyValue $Mon.request_version
						if ($Mon.response_body) {
							$LBMonitorProfile | Add-Member -NotePropertyName "response_body" -NotePropertyValue $Mon.response_body
						}
						if ($Mon.response_status_codes) {
							$LBMonitorProfile | Add-Member -NotePropertyName "response_status_codes" -NotePropertyValue $Mon.response_status_codes
						}
						
					}
					"LbHttpsMonitor" { #not fully supported, check server_ssl_profile_binding in the schema
						$LBMonitorProfile | Add-Member -NotePropertyName "resource_type" -NotePropertyValue "LBHttpsMonitorProfile"
						$LBMonitorProfile | Add-Member -NotePropertyName "fall_count" -NotePropertyValue $Mon.fall_count
						$LBMonitorProfile | Add-Member -NotePropertyName "interval" -NotePropertyValue $Mon.interval 
						if ($Mon.monitor_port) {
							$LBMonitorProfile | Add-Member -NotePropertyName "monitor_port" -NotePropertyValue $Mon.monitor_port
						}
						$LBMonitorProfile | Add-Member -NotePropertyName "rise_count" -NotePropertyValue $Mon.rise_count
						if ($Mon.request_body) {
							$LBMonitorProfile | Add-Member -NotePropertyName "request_body" -NotePropertyValue $Mon.request_body
						}
						if ($Mon.request_headers) {
							$LBMonitorProfile | Add-Member -NotePropertyName "request_headers" -NotePropertyValue $Mon.request_headers
						}
						$LBMonitorProfile | Add-Member -NotePropertyName "request_method" -NotePropertyValue $Mon.request_method
						if ($Mon.request_url) {
							$LBMonitorProfile | Add-Member -NotePropertyName "request_url" -NotePropertyValue $Mon.request_url
						}
						$LBMonitorProfile | Add-Member -NotePropertyName "request_version" -NotePropertyValue $Mon.request_version
						if ($Mon.response_body) {
							$LBMonitorProfile | Add-Member -NotePropertyName "response_body" -NotePropertyValue $Mon.response_body
						}
						if ($Mon.response_status_codes) {
							$LBMonitorProfile | Add-Member -NotePropertyName "response_status_codes" -NotePropertyValue $Mon.response_status_codes
						}
					}
					"LbIcmpMonitor" {
						$LBMonitorProfile | Add-Member -NotePropertyName "resource_type" -NotePropertyValue "LBIcmpMonitorProfile"
						$LBMonitorProfile | Add-Member -NotePropertyName "fall_count" -NotePropertyValue $Mon.fall_count
						$LBMonitorProfile | Add-Member -NotePropertyName "interval" -NotePropertyValue $Mon.interval 
						if ($Mon.monitor_port) {
							$LBMonitorProfile | Add-Member -NotePropertyName "monitor_port" -NotePropertyValue $Mon.monitor_port
						}
						$LBMonitorProfile | Add-Member -NotePropertyName "rise_count" -NotePropertyValue $Mon.rise_count
						if ($Mon.data_length) {
							$LBMonitorProfile | Add-Member -NotePropertyName "data_length" -NotePropertyValue $Mon.data_length
						}
					}
					"LbPassiveMonitor" {
						$LBMonitorProfile | Add-Member -NotePropertyName "resource_type" -NotePropertyValue "LBPassiveMonitorProfile"
						$LBMonitorProfile | Add-Member -NotePropertyName "max_fails" -NotePropertyValue $Mon.max_fails
					}
					"LbTcpMonitor" {
						$LBMonitorProfile | Add-Member -NotePropertyName "resource_type" -NotePropertyValue "LBTcpMonitorProfile"
						$LBMonitorProfile | Add-Member -NotePropertyName "fall_count" -NotePropertyValue $Mon.fall_count
						$LBMonitorProfile | Add-Member -NotePropertyName "interval" -NotePropertyValue $Mon.interval 
						if ($Mon.monitor_port) {
							$LBMonitorProfile | Add-Member -NotePropertyName "monitor_port" -NotePropertyValue $Mon.monitor_port
						}
						$LBMonitorProfile | Add-Member -NotePropertyName "rise_count" -NotePropertyValue $Mon.rise_count
						if ($Mon.receive) {
							$LBMonitorProfile | Add-Member -NotePropertyName "receive" -NotePropertyValue $Mon.receive
						}
						if ($Mon.send) {
							$LBMonitorProfile | Add-Member -NotePropertyName "send" -NotePropertyValue $Mon.send
						}
					}
					"LbUdpMonitor" {
						$LBMonitorProfile | Add-Member -NotePropertyName "resource_type" -NotePropertyValue "LBUdpMonitorProfile"
						$LBMonitorProfile | Add-Member -NotePropertyName "fall_count" -NotePropertyValue $Mon.fall_count
						$LBMonitorProfile | Add-Member -NotePropertyName "interval" -NotePropertyValue $Mon.interval 
						if ($Mon.monitor_port) {
							$LBMonitorProfile | Add-Member -NotePropertyName "monitor_port" -NotePropertyValue $Mon.monitor_port
						}
						$LBMonitorProfile | Add-Member -NotePropertyName "rise_count" -NotePropertyValue $Mon.rise_count
						if ($Mon.receive) {
							$LBMonitorProfile | Add-Member -NotePropertyName "receive" -NotePropertyValue $Mon.receive
						}
						if ($Mon.send) {
							$LBMonitorProfile | Add-Member -NotePropertyName "send" -NotePropertyValue $Mon.send
						}
					}
				}
				$resObj | Add-Member -NotePropertyName "LBMonitorProfile" -NotePropertyValue $LBMonitorProfile ## add monitor to the resulting list
				$result += $resObj
			}
			# if (($Mon._create_user -eq "nsx_policy" ) -and ($Mon._protection -eq "REQUIRE_OVERRIDE")) {
			# 	if ($Mon.tags) {
			# 		if ($Mon.tags[0].scope -eq "policyPath") {
			# 			# $MonitorsHash.add($Mon.id,$Mon.tags[0].tag)
			# 		}
			# 	}
			# }
		}
		$result
	}
}

function New-ClientSSLProfiles {
	param (
		[Parameter(Mandatory=$true)]
			[PSObject]$InObject,
		[Parameter(Mandatory=$false)]
			[switch]$DeleteFlag
	)
	process {
		$result = @()
		foreach ($Obj in $InObject) {
			# init wrapper object
			$resObj = New-Object PSCustomObject
			$resObj | Add-Member -NotePropertyName "resource_type" -NotePropertyValue "ChildLBClientSslProfile"
			if ($DeleteFlag) {
				$resObj | Add-Member -NotePropertyName "marked_for_delete" -NotePropertyValue $true
			}
			### init data object
			$LBClientSslProfile = New-Object PSCustomObject
			## set generic parameters
			if ($DeleteFlag) {
				$LBClientSslProfile | Add-Member -NotePropertyName "marked_for_delete" -NotePropertyValue $true
			}
			$LBClientSslProfile | Add-Member -NotePropertyName "resource_type" -NotePropertyValue "LBClientSslProfile"
			if ($Obj.description) {
				$LBClientSslProfile | Add-Member -NotePropertyName "description" -NotePropertyValue $Obj.description
			}
			$LBClientSslProfile | Add-Member -NotePropertyName "display_name" -NotePropertyValue $Obj.display_name
			$LBClientSslProfile | Add-Member -NotePropertyName "id" -NotePropertyValue $Obj.id 
			if ($Obj.tags) {
				$LBClientSslProfile | Add-Member -NotePropertyName "tags" -NotePropertyValue $Obj.tags
			}
			## set specific parameters
			if ($Obj.cipher_group_label) {
				$LBClientSslProfile | Add-Member -NotePropertyName "cipher_group_label" -NotePropertyValue $Obj.cipher_group_label
			}
			if ($Obj.ciphers) {
				$LBClientSslProfile | Add-Member -NotePropertyName "ciphers" -NotePropertyValue $Obj.ciphers
			}
			$LBClientSslProfile | Add-Member -NotePropertyName "prefer_server_ciphers" -NotePropertyValue $Obj.prefer_server_ciphers
			if ($Obj.protocols) {
				$LBClientSslProfile | Add-Member -NotePropertyName "protocols" -NotePropertyValue $Obj.protocols
			}
			$LBClientSslProfile | Add-Member -NotePropertyName "session_cache_enabled" -NotePropertyValue $Obj.session_cache_enabled
			$LBClientSslProfile | Add-Member -NotePropertyName "session_cache_timeout" -NotePropertyValue $Obj.session_cache_timeout
			## update results
			$resObj | Add-Member -NotePropertyName "LBClientSslProfile" -NotePropertyValue $LBClientSslProfile 
			$result += $resObj
		}
		$result 
	}
}

function New-ServerSSLProfiles {
	param (
		[Parameter(Mandatory=$true)]
			[PSObject]$InObject,
		[Parameter(Mandatory=$false)]
			[switch]$DeleteFlag
	)
	process {
		$result = @()
		foreach ($Obj in $InObject) {
			# init wrapper object
			$resObj = New-Object PSCustomObject
			$resObj | Add-Member -NotePropertyName "resource_type" -NotePropertyValue "ChildLBServerSslProfile"
			if ($DeleteFlag) {
				$resObj | Add-Member -NotePropertyName "marked_for_delete" -NotePropertyValue $true
			}
			### init data object
			$LBServerSslProfile = New-Object PSCustomObject
			## set generic parameters
			if ($DeleteFlag) {
				$LBServerSslProfile | Add-Member -NotePropertyName "marked_for_delete" -NotePropertyValue $true
			}
			$LBServerSslProfile | Add-Member -NotePropertyName "resource_type" -NotePropertyValue "LBServerSslProfile"
			if ($Obj.description) {
				$LBServerSslProfile | Add-Member -NotePropertyName "description" -NotePropertyValue $Obj.description
			}
			$LBServerSslProfile | Add-Member -NotePropertyName "display_name" -NotePropertyValue $Obj.display_name
			$LBServerSslProfile | Add-Member -NotePropertyName "id" -NotePropertyValue $Obj.id 
			if ($Obj.tags) {
				$LBServerSslProfile | Add-Member -NotePropertyName "tags" -NotePropertyValue $Obj.tags
			}
			## set specific parameter
			if ($Obj.cipher_group_label) {
				$LBServerSslProfile | Add-Member -NotePropertyName "cipher_group_label" -NotePropertyValue $Obj.cipher_group_label
			}
			if ($Obj.ciphers) {
				$LBServerSslProfile | Add-Member -NotePropertyName "ciphers" -NotePropertyValue $Obj.ciphers
			}
			if ($Obj.protocols) {
				$LBServerSslProfile | Add-Member -NotePropertyName "protocols" -NotePropertyValue $Obj.protocols
			}
			$LBServerSslProfile | Add-Member -NotePropertyName "session_cache_enabled" -NotePropertyValue $Obj.session_cache_enabled
			## update results
			$resObj | Add-Member -NotePropertyName "LBServerSslProfile" -NotePropertyValue $LBServerSslProfile ## add monitor to the resulting list
			$result += $resObj
		}
		$result 
	}
}

function New-PersistenceProfiles {
	param (
		[Parameter(Mandatory=$true)]
			[PSObject]$InObject,
		[Parameter(Mandatory=$false)]
			[switch]$DeleteFlag
	)
	process {
		$result = @()
		foreach ($Obj in $InObject) {
			# init wrapper object
			$resObj = New-Object PSCustomObject
			$resObj | Add-Member -NotePropertyName "resource_type" -NotePropertyValue "ChildLBPersistenceProfile"
			if ($DeleteFlag) {
				$resObj | Add-Member -NotePropertyName "marked_for_delete" -NotePropertyValue $true
			}
			### init data object
			$LBPersistenceProfile = New-Object PSCustomObject
			## set generic parameters
			if ($DeleteFlag) {
				$LBPersistenceProfile | Add-Member -NotePropertyName "marked_for_delete" -NotePropertyValue $true
			}
			if ($Obj.description) {
				$LBPersistenceProfile | Add-Member -NotePropertyName "description" -NotePropertyValue $Obj.description
			}
			$LBPersistenceProfile | Add-Member -NotePropertyName "display_name" -NotePropertyValue $Obj.display_name
			$LBPersistenceProfile | Add-Member -NotePropertyName "id" -NotePropertyValue $Obj.id 
			if ($Obj.tags) {
				$LBPersistenceProfile | Add-Member -NotePropertyName "tags" -NotePropertyValue $Obj.tags
			}
			## set specific parameter
			switch ($Obj.resource_type) {
				"LbCookiePersistenceProfile" {
					$LBPersistenceProfile | Add-Member -NotePropertyName "resource_type" -NotePropertyValue "LBCookiePersistenceProfile"
					if ($Obj.cookie_domain) {
						$LBPersistenceProfile | Add-Member -NotePropertyName "cookie_domain" -NotePropertyValue $Obj.cookie_domain
					}
					$LBPersistenceProfile | Add-Member -NotePropertyName "cookie_fallback" -NotePropertyValue $Obj.cookie_fallback
					$LBPersistenceProfile | Add-Member -NotePropertyName "cookie_garble" -NotePropertyValue $Obj.cookie_garble
					$LBPersistenceProfile | Add-Member -NotePropertyName "cookie_mode" -NotePropertyValue $Obj.cookie_mode
					$LBPersistenceProfile | Add-Member -NotePropertyName "cookie_name" -NotePropertyValue $Obj.cookie_name
					if ($Obj.cookie_path) {
						$LBPersistenceProfile | Add-Member -NotePropertyName "cookie_path" -NotePropertyValue $Obj.cookie_path
					}
					if ($Obj.cookie_time) {
						$CookieTime = New-Object PSCustomObject
						if ($Obj.cookie_time.type -eq "LbPersistenceCookieTime") {
							$CookieTime | Add-Member -NotePropertyName "type" -NotePropertyValue "LBPersistenceCookieTime"
							$CookieTime | Add-Member -NotePropertyName "cookie_max_idle" -NotePropertyValue $Obj.cookie_time.cookie_max_idle
						}
						if ($Obj.cookie_time.type -eq "LbSessionCookieTime") {
							$CookieTime | Add-Member -NotePropertyName "type" -NotePropertyValue "LBSessionCookieTime"
							if ($Obj.cookie_time.cookie_max_idle) {
								$CookieTime | Add-Member -NotePropertyName "cookie_max_idle" -NotePropertyValue $Obj.cookie_time.cookie_max_idle
							}
							if ($Obj.cookie_time.cookie_max_life) {
								$CookieTime | Add-Member -NotePropertyName "cookie_max_life" -NotePropertyValue $Obj.cookie_time.cookie_max_life
							}
						}
						$LBPersistenceProfile | Add-Member -NotePropertyName "cookie_time" -NotePropertyValue $CookieTime
					}
					$LBPersistenceProfile | Add-Member -NotePropertyName "persistence_shared" -NotePropertyValue $Obj.persistence_shared
				}
				"LbGenericPersistenceProfile" {
					$LBPersistenceProfile | Add-Member -NotePropertyName "resource_type" -NotePropertyValue "LBGenericPersistenceProfile"
					$LBPersistenceProfile | Add-Member -NotePropertyName "ha_persistence_mirroring_enabled" -NotePropertyValue $Obj.ha_persistence_mirroring_enabled
					$LBPersistenceProfile | Add-Member -NotePropertyName "persistence_shared" -NotePropertyValue $Obj.persistence_shared
					$LBPersistenceProfile | Add-Member -NotePropertyName "timeout" -NotePropertyValue $Obj.timeout
				}
				"LbSourceIpPersistenceProfile" {
					$LBPersistenceProfile | Add-Member -NotePropertyName "resource_type" -NotePropertyValue "LBSourceIpPersistenceProfile"
					$LBPersistenceProfile | Add-Member -NotePropertyName "ha_persistence_mirroring_enabled" -NotePropertyValue $Obj.ha_persistence_mirroring_enabled
					$LBPersistenceProfile | Add-Member -NotePropertyName "persistence_shared" -NotePropertyValue $Obj.persistence_shared
					$LBPersistenceProfile | Add-Member -NotePropertyName "purge" -NotePropertyValue $Obj.purge
					$LBPersistenceProfile | Add-Member -NotePropertyName "timeout" -NotePropertyValue $Obj.timeout
				}
			} 
			## update results
			$resObj | Add-Member -NotePropertyName "LBPersistenceProfile" -NotePropertyValue $LBPersistenceProfile ## add monitor to the resulting list
			$result += $resObj
		}
		$result 
	}
}

function New-ApplicationProfiles {
	param (
		[Parameter(Mandatory=$true)]
			[PSObject]$InObject,
		[Parameter(Mandatory=$false)]
			[switch]$DeleteFlag
	)
	process {
		$result = @()
		foreach ($Obj in $InObject) {
			# init wrapper object
			$resObj = New-Object PSCustomObject
			$resObj | Add-Member -NotePropertyName "resource_type" -NotePropertyValue "ChildLBAppProfile"
			if ($DeleteFlag) {
				$resObj | Add-Member -NotePropertyName "marked_for_delete" -NotePropertyValue $true
			}
			### init data object
			$LBAppProfile = New-Object PSCustomObject
			## set generic parameters
			if ($DeleteFlag) {
				$LBAppProfile | Add-Member -NotePropertyName "marked_for_delete" -NotePropertyValue $true
			}
			if ($Obj.description) {
				$LBAppProfile | Add-Member -NotePropertyName "description" -NotePropertyValue $Obj.description
			}
			$LBAppProfile | Add-Member -NotePropertyName "display_name" -NotePropertyValue $Obj.display_name
			$LBAppProfile | Add-Member -NotePropertyName "id" -NotePropertyValue $Obj.id 
			if ($Obj.tags) {
				$LBAppProfile | Add-Member -NotePropertyName "tags" -NotePropertyValue $Obj.tags
			}
			## set specific parameter
			switch ($Obj.resource_type) {
				"LbFastTcpProfile" {
					$LBAppProfile | Add-Member -NotePropertyName "resource_type" -NotePropertyValue "LBFastTcpProfile"
					$LBAppProfile | Add-Member -NotePropertyName "close_timeout" -NotePropertyValue $Obj.close_timeout
					$LBAppProfile | Add-Member -NotePropertyName "ha_flow_mirroring_enabled" -NotePropertyValue $Obj.ha_flow_mirroring_enabled
					$LBAppProfile | Add-Member -NotePropertyName "idle_timeout" -NotePropertyValue $Obj.idle_timeout
				}
				"LbFastUdpProfile" {
					$LBAppProfile | Add-Member -NotePropertyName "resource_type" -NotePropertyValue "LBFastUdpProfile"
					$LBAppProfile | Add-Member -NotePropertyName "flow_mirroring_enabled" -NotePropertyValue $Obj.flow_mirroring_enabled
					$LBAppProfile | Add-Member -NotePropertyName "idle_timeout" -NotePropertyValue $Obj.idle_timeout
				}
				"LbHttpProfile" {
					$LBAppProfile | Add-Member -NotePropertyName "resource_type" -NotePropertyValue "LBHttpProfile"
					if ($Obj.http_redirect_to) {
						$LBAppProfile | Add-Member -NotePropertyName "http_redirect_to" -NotePropertyValue $Obj.http_redirect_to
					}
					$LBAppProfile | Add-Member -NotePropertyName "http_redirect_to_https" -NotePropertyValue $Obj.http_redirect_to_https
					$LBAppProfile | Add-Member -NotePropertyName "idle_timeout" -NotePropertyValue $Obj.idle_timeout
					$LBAppProfile | Add-Member -NotePropertyName "ntlm" -NotePropertyValue $Obj.ntlm
					if ($Obj.request_body_size) {
						$LBAppProfile | Add-Member -NotePropertyName "request_body_size" -NotePropertyValue $Obj.request_body_size
					}
					$LBAppProfile | Add-Member -NotePropertyName "request_header_size" -NotePropertyValue $Obj.request_header_size
					# $LBAppProfile | Add-Member -NotePropertyName "response_buffering" -NotePropertyValue $Obj.response_buffering
					$LBAppProfile | Add-Member -NotePropertyName "response_header_size" -NotePropertyValue $Obj.response_header_size
					$LBAppProfile | Add-Member -NotePropertyName "response_timeout" -NotePropertyValue $Obj.response_timeout
					if ($Obj.x_forwarded_for) {
						$LBAppProfile | Add-Member -NotePropertyName "x_forwarded_for" -NotePropertyValue $Obj.x_forwarded_for
					}
				}
			}
			## update results
			$resObj | Add-Member -NotePropertyName "LBAppProfile" -NotePropertyValue $LBAppProfile ## add monitor to the resulting list
			$result += $resObj
		}
		$result 
	}
}

function New-Certificates {
	param (
		[Parameter(Mandatory=$true)]
			[PSObject]$InObject,
		[Parameter(Mandatory=$true)]
			[PSObject]$Connection
	)
	process {
		foreach ($Obj in $InObject) {
			$TlsTrustData = New-Object PSCustomObject
			$TlsTrustData | Add-Member -NotePropertyName "resource_type" -NotePropertyValue "TlsTrustData"
			if ($Obj.description) {
				$TlsTrustData | Add-Member -NotePropertyName "description" -NotePropertyValue $Obj.description
			}
			$TlsTrustData | Add-Member -NotePropertyName "display_name" -NotePropertyValue $Obj.display_name
			$TlsTrustData | Add-Member -NotePropertyName "id" -NotePropertyValue $Obj.id 
			if ($Obj.tags) {
				$TlsTrustData | Add-Member -NotePropertyName "tags" -NotePropertyValue $Obj.tags
			}
			$TlsTrustData | Add-Member -NotePropertyName "pem_encoded" -NotePropertyValue $Obj.pem_encoded
			$URI = "/policy/api/v1/infra/certificates/$($Obj.id)"
			$result = Invoke-NSXRestAPICall -Connection $Connection -URI $URI -Method "patch" -JSONBody ($TlsTrustData | ConvertTo-Json -Depth 15) -ErrorAction SilentlyContinue
		}
	}
}

function Remove-Certificates {
	param (
		[Parameter(Mandatory=$true)]
			[PSObject]$InObject,
		[Parameter(Mandatory=$true)]
			[PSObject]$Connection
	)
	process {
		foreach ($Obj in $InObject) {
			$URI = "/policy/api/v1/infra/certificates/$($Obj.id)"
			$result = Invoke-NSXRestAPICall -Connection $Connection -URI $URI -Method "DELETE" -ErrorAction SilentlyContinue
		}
	}	
}

function New-Pools {
	param (
		[Parameter(Mandatory=$true)]
			[PSObject]$InObject,
		[Parameter(Mandatory=$false)]
			[switch]$DeleteFlag
	)
	process {
		$result = @()
		foreach ($Obj in $InObject) {
			# init wrapper object
			$resObj = New-Object PSCustomObject
			$resObj | Add-Member -NotePropertyName "resource_type" -NotePropertyValue "ChildLBPool"
			if ($DeleteFlag) {
				$resObj | Add-Member -NotePropertyName "marked_for_delete" -NotePropertyValue $true
			}
			### init data object
			$LBPool = New-Object PSCustomObject
			## set generic parameters
			if ($DeleteFlag) {
				$LBPool | Add-Member -NotePropertyName "marked_for_delete" -NotePropertyValue $true
			}
			$LBPool | Add-Member -NotePropertyName "resource_type" -NotePropertyValue "LBPool"
			if ($Obj.description) {
				$LBPool | Add-Member -NotePropertyName "description" -NotePropertyValue $Obj.description
			}
			$LBPool | Add-Member -NotePropertyName "display_name" -NotePropertyValue $Obj.display_name
			$LBPool | Add-Member -NotePropertyName "id" -NotePropertyValue $Obj.id 
			if ($Obj.tags) {
				$LBPool | Add-Member -NotePropertyName "tags" -NotePropertyValue $Obj.tags
			}
			## set specific parameters
			if ($Obj.active_monitor_ids) {
				$active_monitor_paths = @()
				foreach ($id in $Obj.active_monitor_ids) {
					$active_monitor_paths += "/infra/lb-monitor-profiles/$($id)"
				}
				$LBPool | Add-Member -NotePropertyName "active_monitor_paths" -NotePropertyValue $active_monitor_paths
			}
			$LBPool | Add-Member -NotePropertyName "algorithm" -NotePropertyValue $Obj.algorithm
			# member_group - dynamic pool membership is not supported
			if ($Obj.members) {
				$LBPool | Add-Member -NotePropertyName "members" -NotePropertyValue $Obj.members
			}
			$LBPool | Add-Member -NotePropertyName "min_active_members" -NotePropertyValue $Obj.min_active_members
			if ($Obj.passive_monitor_id) {
				$LBPool | Add-Member -NotePropertyName "passive_monitor_path" -NotePropertyValue "/infra/lb-monitor/profiles/$($Obj.passive_monitor_id)"
			}
			if ($Obj.snat_translation) {
				$SNATTranslation = New-Object PSCustomObject
				if ($Obj.snat_translation.type -eq "LbSnatAutoMap") {
					$SNATTranslation | Add-Member -NotePropertyName "type" -NotePropertyValue "LBSnatAutoMap"
				}
				if ($Obj.snat_translation.type -eq "LbSnatIpPool") {
					$SNATTranslation | Add-Member -NotePropertyName "ip_addresses" -NotePropertyValue $Obj.snat_translation.ip_addresses
					$SNATTranslation | Add-Member -NotePropertyName "type" -NotePropertyValue "LBSnatIpPool"
				}
				$LBPool | Add-Member -NotePropertyName "snat_translation" -NotePropertyValue $SNATTranslation
			}
			$LBPool | Add-Member -NotePropertyName "tcp_multiplexing_enabled" -NotePropertyValue $Obj.tcp_multiplexing_enabled
			$LBPool | Add-Member -NotePropertyName "tcp_multiplexing_number" -NotePropertyValue $Obj.tcp_multiplexing_number
			## update results
			$resObj | Add-Member -NotePropertyName "LBPool" -NotePropertyValue $LBPool ## add monitor to the resulting list
			$result += $resObj
		}
		$result 
	}
}

function New-VirtualServers {
	param (
		[Parameter(Mandatory=$true)]
			[PSObject]$InObject,
		[Parameter(Mandatory=$false)]
			[switch]$DeleteFlag
	)
	process {
		$result = @()
		foreach ($Obj in $InObject) {
			# init wrapper object
			$resObj = New-Object PSCustomObject
			$resObj | Add-Member -NotePropertyName "resource_type" -NotePropertyValue "ChildLBVirtualServer"
			if ($DeleteFlag) {
				$resObj | Add-Member -NotePropertyName "marked_for_delete" -NotePropertyValue $true
			}
			### init data object
			$LBVirtualServer = New-Object PSCustomObject
			## set generic parameters
			if ($DeleteFlag) {
				$LBVirtualServer | Add-Member -NotePropertyName "marked_for_delete" -NotePropertyValue $true
			}
			$LBVirtualServer | Add-Member -NotePropertyName "resource_type" -NotePropertyValue "LBVirtualServer"
			if ($Obj.description) {
				$LBVirtualServer | Add-Member -NotePropertyName "description" -NotePropertyValue $Obj.description
			}
			$LBVirtualServer | Add-Member -NotePropertyName "display_name" -NotePropertyValue $Obj.display_name
			$LBVirtualServer | Add-Member -NotePropertyName "id" -NotePropertyValue $Obj.id 
			if ($Obj.tags) {
				$LBVirtualServer | Add-Member -NotePropertyName "tags" -NotePropertyValue $Obj.tags
			}
			## set specific parameters
			# access_list_control  - doesn't seem there is a way to configure it from the UI. Unsopported
			$LBVirtualServer | Add-Member -NotePropertyName "access_log_enabled" -NotePropertyValue $Obj.access_log_enabled
			$LBVirtualServer | Add-Member -NotePropertyName "application_profile_path" -NotePropertyValue "/infra/lb-app-profiles/$($Obj.application_profile_id)"
			# client_ssl_profile_binding requires certificates with private keys
			# if ($Obj.client_ssl_profile_binding) {
			# 	$ClientSSLProfileBinding = New-Object PSCustomObject
			# 	$ClientSSLProfileBinding | Add-Member -NotePropertyName "certificate_chain_depth" -NotePropertyValue $Obj.client_ssl_profile_binding.certificate_chain_depth
			# 	$ClientSSLProfileBinding | Add-Member -NotePropertyName "client_auth" -NotePropertyValue $Obj.client_ssl_profile_binding.client_auth
			# 	if ($Obj.client_ssl_profile_binding.client_auth_ca_ids) {
			# 		$client_auth_ca_paths = @()
			# 		foreach ($id in $Obj.client_ssl_profile_binding.client_auth_ca_ids) {
			# 			$client_auth_ca_paths += "/infra/certificates/$($id)"
			# 		}
			# 		$ClientSSLProfileBinding | Add-Member -NotePropertyName "client_auth_ca_paths" -NotePropertyValue $client_auth_ca_paths
			# 	}
			# 	# no support for CRLs yet
			# 	# if ($Obj.client_ssl_profile_binding.client_auth_crl_ids) {
			# 	# 	$client_auth_crl_paths = @()
			# 	# 	foreach ($id in $Obj.client_ssl_profile_binding.client_auth_crl_ids) {
			# 	# 		$client_auth_crl_paths += "/infra/crls/$($id)"
			# 	# 	}
			# 	# 	$ClientSSLProfileBinding | Add-Member -NotePropertyName "client_auth_crl_paths" -NotePropertyValue $client_auth_crl_paths
			# 	# }
			# 	$ClientSSLProfileBinding | Add-Member -NotePropertyName "default_certificate_path" -NotePropertyValue "/infra/certificates/$($Obj.client_ssl_profile_binding.default_certificate_id)"
			# 	if ($Obj.client_ssl_profile_binding.ssl_profile_id) {
			# 		$ClientSSLProfileBinding | Add-Member -NotePropertyName "ssl_profile_path" -NotePropertyValue "/infra/lb-client-ssl-profiles/$($Obj.client_ssl_profile_binding.ssl_profile_id)"
			# 	}
			# 	$LBVirtualServer | Add-Member -NotePropertyName "client_ssl_profile_binding" -NotePropertyValue $ClientSSLProfileBinding
			# }
			if ($Obj.default_pool_member_ports) {
				$LBVirtualServer | Add-Member -NotePropertyName "default_pool_member_ports" -NotePropertyValue $Obj.default_pool_member_ports
			}
			$LBVirtualServer | Add-Member -NotePropertyName "enabled" -NotePropertyValue $Obj.enabled
			$LBVirtualServer | Add-Member -NotePropertyName "ip_address" -NotePropertyValue $Obj.ip_address
			if ($Obj.persistence_profile_id) {
				$LBVirtualServer | Add-Member -NotePropertyName "lb_persistence_profile_path" -NotePropertyValue "/infra/lb-persistence-profiles/$($Obj.persistence_profile_id)"
			}
			if ($Obj.lb_service_path) {
				$LBVirtualServer | Add-Member -NotePropertyName "lb_service_path" -NotePropertyValue $Obj.lb_service_path
			}
			# $LBVirtualServer | Add-Member -NotePropertyName "log_significant_event_only" -NotePropertyValue $Obj.log_significant_event_only
			if ($Obj.max_concurrent_connections) {
				$LBVirtualServer | Add-Member -NotePropertyName "max_concurrent_connections" -NotePropertyValue $Obj.max_concurrent_connections
			}
			if ($Obj.max_new_connection_rate) {
				$LBVirtualServer | Add-Member -NotePropertyName "max_new_connection_rate" -NotePropertyValue $Obj.max_new_connection_rate
			}
			if ($Obj.pool_id) {
				$LBVirtualServer | Add-Member -NotePropertyName "pool_path" -NotePropertyValue "/infra/lb-pools/$($Obj.pool_id)"
			}
			$LBVirtualServer | Add-Member -NotePropertyName "ports" -NotePropertyValue $Obj.ports
			# rules - looks like customer doesn't use rules - so no support yet
			# server_ssl_profile_binding requires client_ssl_profile_binding as well 
			# if ($Obj.server_ssl_profile_binding) {
			# 	$ServerSSLProfileBinding = New-Object PSCustomObject
			# 	$ServerSSLProfileBinding | Add-Member -NotePropertyName "certificate_chain_depth" -NotePropertyValue $Obj.server_ssl_profile_binding.certificate_chain_depth
			# 	if ($Obj.server_ssl_profile_binding.client_certificate_id) {
			# 		$ServerSSLProfileBinding | Add-Member -NotePropertyName "client_certificate_path" -NotePropertyValue "/infra/certificates/$($Obj.server_ssl_profile_binding.client_certificate_id)"
			# 	}
			# 	$ServerSSLProfileBinding | Add-Member -NotePropertyName "server_auth" -NotePropertyValue $Obj.server_ssl_profile_binding.server_auth
			# 	if ($Obj.server_ssl_profile_binding.server_auth_ca_ids) {
			# 		$server_auth_ca_paths = @()
			# 		foreach ($id in $Obj.server_ssl_profile_binding.server_auth_ca_ids) {
			# 			$server_auth_ca_paths += "/infra/certificates/$($id)"
			# 		}
			# 		$ServerSSLProfileBinding | Add-Member -NotePropertyName "server_auth_ca_paths" -NotePropertyValue $server_auth_ca_paths
			# 	}
			# 	# no support for CRLs yet
			# 	# if ($Obj.server_ssl_profile_binding.server_auth_crl_ids) {
			# 	# 	$server_auth_crl_paths = @()
			# 	# 	foreach ($id in $Obj.server_ssl_profile_binding.client_auth_crl_ids) {
			# 	# 		$server_auth_crl_paths += "/infra/crls/$($id)"
			# 	# 	}
			# 	# 	$ServerSSLProfileBinding | Add-Member -NotePropertyName "server_auth_crl_paths" -NotePropertyValue $server_auth_crl_paths
			# 	# }
			# 	if ($Obj.server_ssl_profile_binding.ssl_profile_id) {
			# 		$ServerSSLProfileBinding | Add-Member -NotePropertyName "ssl_profile_path" -NotePropertyValue "/infra/lb-server-ssl-profiles/$($Obj.server_ssl_profile_binding.ssl_profile_id)"
			# 	}
			# 	$LBVirtualServer | Add-Member -NotePropertyName "server_ssl_profile_binding" -NotePropertyValue $ServerSSLProfileBinding
			# }
			if ($Obj.sorry_pool_id) {
				$LBVirtualServer | Add-Member -NotePropertyName "sorry_pool_path" -NotePropertyValue "/infra/lb-pools/$($Obj.sorry_pool_id)"
			}
			## update results
			$resObj | Add-Member -NotePropertyName "LBVirtualServer" -NotePropertyValue $LBVirtualServer ## add monitor to the resulting list
			$result += $resObj
		}
		$result 
	}
}

function New-LoadBalancers {
	param (
		[Parameter(Mandatory=$true)]
			[PSObject]$InObject,
		[Parameter(Mandatory=$false)]
			[switch]$DeleteFlag,
		[Parameter(Mandatory=$false)]
			[switch]$EnforceDisabled	
	)
	process {
		$result = @()
		foreach ($Obj in $InObject) {
			# init wrapper object
			$resObj = New-Object PSCustomObject
			$resObj | Add-Member -NotePropertyName "resource_type" -NotePropertyValue "ChildLBService"
			if ($DeleteFlag) {
				$resObj | Add-Member -NotePropertyName "marked_for_delete" -NotePropertyValue $true
			}
			### init data object
			$LBService = New-Object PSCustomObject
			## set generic parameters
			if ($DeleteFlag) {
				$LBService | Add-Member -NotePropertyName "marked_for_delete" -NotePropertyValue $true
			}
			$LBService | Add-Member -NotePropertyName "resource_type" -NotePropertyValue "LBService"
			if ($Obj.description) {
				$LBService | Add-Member -NotePropertyName "description" -NotePropertyValue $Obj.description
			}
			$LBService | Add-Member -NotePropertyName "display_name" -NotePropertyValue $Obj.display_name
			$LBService | Add-Member -NotePropertyName "id" -NotePropertyValue $Obj.id 
			if ($Obj.tags) {
				$LBService | Add-Member -NotePropertyName "tags" -NotePropertyValue $Obj.tags
			}
			## set specific parameters
			# connectivity_path - not supported
			if ($EnforceDisabled) {
				$LBService | Add-Member -NotePropertyName "enabled" -NotePropertyValue $false
			}
			else {
				$LBService | Add-Member -NotePropertyName "enabled" -NotePropertyValue $Obj.enabled
			}
			$LBService | Add-Member -NotePropertyName "error_log_level" -NotePropertyValue $Obj.error_log_level
			# $LBService | Add-Member -NotePropertyName "relax_scale_validation" -NotePropertyValue $Obj.relax_scale_validation
			$LBService | Add-Member -NotePropertyName "size" -NotePropertyValue $Obj.size
			## update results
			$resObj | Add-Member -NotePropertyName "LBService" -NotePropertyValue $LBService ## add monitor to the resulting list
			$result += $resObj
		}
		$result 
	}
}

# function New-Objects {
# 	param (
# 		[Parameter(Mandatory=$true)]
# 			[PSObject]$InObject,
# 		[Parameter(Mandatory=$false)]
# 			[switch]$DeleteFlag
# 	)
# 	process {
# 		$result = @()
# 		foreach ($Obj in $InObject) {
# 			# init wrapper object
# 			$resObj = New-Object PSCustomObject
# 			$resObj | Add-Member -NotePropertyName "resource_type" -NotePropertyValue "ChildLBMonitorProfile"
# 			if ($DeleteFlag) {
# 				$resObj | Add-Member -NotePropertyName "marked_for_delete" -NotePropertyValue $true
# 			}
# 			### init data object
# 			$LBMonitorProfile = New-Object PSCustomObject
# 			## set generic parameters
# 			if ($DeleteFlag) {
# 				$LBMonitorProfile | Add-Member -NotePropertyName "marked_for_delete" -NotePropertyValue $true
# 			}
# 			## set specific parameters

# 			## update results
# 			$resObj | Add-Member -NotePropertyName "LBMonitorProfile" -NotePropertyValue $LBMonitorProfile ## add monitor to the resulting list
# 			$result += $resObj
# 		}
# 		$result 
# 	}
# }

# 
# 
# 
# 
###################
############ CONFIG
###################
$UserIN = "admin"
$PasswordIN = "VMware1!VMware1!"
$NSXManagerIN = "10.123.18.54:4443"

$UserOUT = "admin"
$PasswordOUT = "VMware1!VMware1!"
$NSXManagerOUT = "10.248.118.250"

$GlobalDeleteFlag = $true
###################
###################
###################

try {
	$NSXConnection = Connect-NSXTManager -Server $NSXManagerIN -Username $UserIN -Password $PasswordIN
}
catch {
	throw "Something went wrong. $_"   
}



try {
	$MgmtAPILBs = Get-ManagementAPIObjects -URI "/api/v1/loadbalancer/services" -FilterOutPolicy
	$MgmtAPIVIPs = Get-ManagementAPIObjects -URI "/api/v1/loadbalancer/virtual-servers" -FilterOutPolicy
	$AppProfiles = Get-ManagementAPIObjects -URI "/api/v1/loadbalancer/application-profiles" #-FilterOutPolicy
	$PersistenceProfiles = Get-ManagementAPIObjects -URI "/api/v1/loadbalancer/persistence-profiles" #-FilterOutPolicy
	$ClientSSLProfiles = Get-ManagementAPIObjects -URI "/api/v1/loadbalancer/client-ssl-profiles" #-FilterOutPolicy
	$ServerSSLProfiles = Get-ManagementAPIObjects -URI "/api/v1/loadbalancer/server-ssl-profiles" #-FilterOutPolicy
	$MgmtAPILBRules = Get-ManagementAPIObjects -URI "/api/v1/loadbalancer/rules" -FilterOutPolicy # We don't do any LB Rules modification/creation yet
	$MgmtAPIPools = Get-ManagementAPIObjects -URI "/api/v1/loadbalancer/pools" -FilterOutPolicy
	$Monitors = Get-ManagementAPIObjects -URI "/api/v1/loadbalancer/monitors" -FilterOutPolicy
	$Certificates = Get-ManagementAPIObjects -URI "/api/v1/trust-management/certificates" #-FilterOutPolicy
	echo "Load Balancers: $($MgmtAPILBs.length)"
	echo "Virtual Servers: $($MgmtAPIVIPs.length)" 
	echo "Application Profiles: $($AppProfiles.length)" 
	echo "Persistence Profiles: $($PersistenceProfiles.length)" 
	echo "Client SSL Profiles: $($ClientSSLProfiles.length)" 
	echo "Server SSL Profiles: $($ServerSSLProfiles.length)" 
	echo "Load Balancer Rules: $($MgmtAPILBRules.length)"
	echo "Server Pools: $($MgmtAPIPools.length)" 
	echo "Monitors: $($Monitors.length)" 
	echo "Certificates: $($Certificates.length)" 
}
catch {
    throw "Something went wrong. $_"
}

# Management API: LB object hold links to Virtual Servers
# Policy API: Virtual Server object hold links to LB 
# Here we update internal $MgmtAPIVIPs object with required data 

foreach ($LB in $MgmtAPILBs) {
	if ($LB.virtual_server_ids) {
		foreach ($vs_id in $LB.virtual_server_ids) {
			($MgmtAPIVIPs | Where-Object { $_.id -eq $vs_id} ) | Add-Member -NotePropertyName "lb_service_path" -NotePropertyValue "/infra/lb-services/$($LB.id)"
		}
	}
}

try {
	$NSXConnection = Connect-NSXTManager -Server $NSXManagerOUT -Username $UserOUT -Password $PasswordOUT
}
catch {
	throw "Something went wrong. $_"   
}

### can't conserve certificate private keys this way
if ($GlobalDeleteFlag) {
	Remove-Certificates -InObject $Certificates -Connection $NSXConnection
}
else {
	New-Certificates -InObject $Certificates -Connection $NSXConnection
}

$emptyArray = @()
$result = $null 
$result = New-Object PSCustomObject
$result | Add-Member -NotePropertyName "resource_type" -NotePropertyValue "Infra"
$result | Add-Member -NotePropertyName "children" -NotePropertyValue $emptyArray
$result.children += New-Monitors -InObject $Monitors -DeleteFlag:$GlobalDeleteFlag # Management API HTTPS monitors are not fully supported
$result.children += New-ClientSSLProfiles -InObject $ClientSSLProfiles -DeleteFlag:$GlobalDeleteFlag
$result.children += New-ServerSSLProfiles -InObject $ServerSSLProfiles -DeleteFlag:$GlobalDeleteFlag
$result.children += New-PersistenceProfiles -InObject $PersistenceProfiles -DeleteFlag:$GlobalDeleteFlag
$result.children += New-ApplicationProfiles -InObject $AppProfiles -DeleteFlag:$GlobalDeleteFlag
$result.children += New-Pools -InObject $MgmtAPIPools -DeleteFlag:$GlobalDeleteFlag # Group-based pool membership is not supported
$result.children += New-VirtualServers -InObject $MgmtAPIVIPs -DeleteFlag:$GlobalDeleteFlag # SSL Profiles are not supported. Rules are not supported. access_list_control is not supported
$result.children += New-LoadBalancers -InObject $MgmtAPILBs -DeleteFlag:$GlobalDeleteFlag -EnforceDisabled # LBs are created disconencted from any T1

$URI = "/policy/api/v1/infra/"
Invoke-NSXRestAPICall -Connection $NSXConnection -URI $URI -Method "patch" -JSONBody ($result | ConvertTo-Json -Depth 15)


