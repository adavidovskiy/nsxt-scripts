<#
.SYNOPSIS
Replicates DFW rules from the source NSX-T to the destination NSX-T

.PARAMETER NSXManagerIN
FQDN or IP of the source NSX-T Manager

.PARAMETER UserIN
Source system user. Tested only with "admin". Defaults to "admin" if nothing is provided

.PARAMETER PasswordIN
Source system password

.PARAMETER NSXManagerOUT
FQDN or IP of the destination NSX-T Manager

.PARAMETER UserOUT
Destination system user. Tested only with "admin". Defaults to "admin" if nothing is provided

.PARAMETER PasswordOUT
Destination system password
#>

param (
    [Parameter(Mandatory=$true)]
        [string]$NSXManagerIN,
    [Parameter(Mandatory=$false)]
        [string]$UserIN,
    [Parameter(Mandatory=$false)]
        [string]$PasswordIN,
    [Parameter(Mandatory=$true)]
        [string]$NSXManagerOUT,
    [Parameter(Mandatory=$false)]
        [string]$UserOUT,
    [Parameter(Mandatory=$false)]
        [string]$PasswordOUT
)

function Connect-NSXTManager {

	<#
	.SYNOPSIS
	Connects to NSX-T Manager
	
	.DESCRIPTION
	As REST API is not connection-oriented, we can't actually connect to server and keep connection.
	Instead, are trying to create some proxy connection object to use in following calls
	
	.PARAMETER Server
	The FQDN or IP of your NSX Manager
	
	.PARAMETER Username
	The username to connect with. Defaults to admin if nothing is provided.
	
	.PARAMETER Password
	The password to connect with
	
	.PARAMETER DefaultConnection
	Will be used as default connection for API calls to follow, if set to true. Default is True
	
	.EXAMPLE
	PS> Connect-NSXTManager -Server "nsxmgr.corp.local" -Username admin -Password passwd123
	#>
	
	[CmdletBinding()] 
	param (
		[Parameter(Mandatory=$true,Position=0)]
			[String]$Server,
		[Parameter(Mandatory=$false,Position=1)]
			[String]$Username = "admin",
		[Parameter(Mandatory=$true)]
			[String]$Pwd,
		[Parameter(Mandatory=$false)]
			[Bool]$DefaultConnection=$true
	)
	
	process {
        $URI = "/api/v1/global-configs"
		$cred = New-Object System.Management.Automation.PSCredential($Username, $(ConvertTo-SecureString $Pwd -AsPlainText -Force))
		try {
			Invoke-NSXRestAPICall -Cred $cred -Server $Server -URI $URI -Method "get" | Out-Null
		}
		catch {
			Throw "Unable to connect NSX Manager $NSXManager. $_"
		}
		$connection = New-Object PSCustomObject
		$connection | Add-Member -MemberType NoteProperty -Force -Name "Server" -Value $Server
		$connection | Add-Member -MemberType NoteProperty -Force -Name "Credential" -Value $cred
		
		if ($DefaultConnection) {
			Set-Variable -Name DefaultNSXConnection -Value $connection -Scope Global
		}
		$connection
	}
}
function Invoke-NSXRestAPICall {

	#Internal method to construct the REST call headers including auth as expected by NSX.
    #Accepts either a connection object as produced by Connect-NSXServer or explicit
    #parameters.
	#Ripped from PowerCLI module, simplified and adapted for NSX-T
	
	[CmdletBinding(DefaultParameterSetName="ConnectionSet")]
	param (
		[Parameter(Mandatory=$true,ParameterSetName="ManualSet")]
			[System.Management.Automation.PSCredential]$Cred,
		[Parameter(Mandatory=$true,ParameterSetName="ManualSet")]
			[String]$Server,
		[Parameter(Mandatory=$true,ParameterSetName="ManualSet")]
		[Parameter(ParameterSetName="ConnectionSet")]
			[String]$Method,
		[Parameter(Mandatory=$true,ParameterSetName="ManualSet")]
		[Parameter(ParameterSetName="ConnectionSet")]
			[String]$URI,
		[Parameter(Mandatory=$false,ParameterSetName="ManualSet")]
		[Parameter(ParameterSetName="ConnectionSet")]
			[String]$JSONBody,
		[Parameter(Mandatory=$false,ParameterSetName="ManualSet")]
		[Parameter(ParameterSetName="ConnectionSet")]
			[bool]$SkipCertificateCheck=$true,
		[Parameter(Mandatory=$false,ParameterSetName="ManualSet")]
		[Parameter(ParameterSetName="ConnectionSet")]
			[bool]$DisableKeepAlive=$false,
		[Parameter(Mandatory=$false,ParameterSetName="ConnectionSet")]
			[psObject]$Connection
	)
	
	process {
		if ($PSCmdlet.ParameterSetName -eq "ConnectionSet") {
			if ($Connection -eq $null) {
				#Now we need to assume that defaultnsxconnection does not exist...
	            if ( -not (test-path variable:global:DefaultNSXConnection) ) { 
	                throw "Not connected.  Connect to NSX manager with Connect-NsxServer first." 
	            }
	            else { 
	                Write-Debug "$($MyInvocation.MyCommand.Name) : Using default connection"
	                $Connection = $DefaultNSXConnection
	            }   
			}
			
			$Cred = $Connection.Credential
			$Server = $Connection.Server
		}
		
		$base64cred = [System.Convert]::ToBase64String(
			[System.Text.Encoding]::UTF8.GetBytes(
				$Cred.GetNetworkCredential().UserName + ":" + $Cred.GetNetworkCredential().password
			)
		)
		$headerDictionary = @{"Authorization"="Basic $base64cred"}
						
		$request = "https://$($Server)$($URI)"  # equals "https://" + $Server + $URI
		
		try {
			if ($Method -eq "patch") {
				$response = Invoke-RestMethod -Uri $request -Headers $headerDictionary -ContentType "application/json" -Method $Method -Body $JSONBody -DisableKeepAlive:$DisableKeepAlive -SkipCertificateCheck:$SkipCertificateCheck
			}
			else {
				$response = Invoke-RestMethod -Uri $request -Headers $headerDictionary -ContentType "application/json" -Method $Method -DisableKeepAlive:$DisableKeepAlive -SkipCertificateCheck:$SkipCertificateCheck
			}
		}
		catch {
            $response = $_.Exception.Response
			$statusCode = $_.Exception.Response.StatusCode.value__
			$messageObj = New-Object PSCustomObject
			$messageObj = $_.ErrorDetails.Message | ConvertFrom-Json
			$errorMessage = $messageObj.error_message
			$errorString = "Invoke-NSXRestAPICall: Status code: $($statusCode). Error message: $($errorMessage) Method: $($Method) URI: $($URI)"
			foreach ($err in $messageObj.related_errors) {
				$errorString += "`n"
				$errorString += $err.error_message
			}
            throw $errorString
            # $errorMessage = ($_.ErrorDetails.Message | ConvertFrom-Json).error_message
            # $errorString = "Invoke-NSXRestAPICall: Status code: $($statusCode). Error message: $($errorMessage) Method: $($Method) URI: $($URI)"
            # throw $errorString
        }
		$response
	}
}

function Remove-SystemOwnedObjects {
    param (
        [Parameter(Mandatory=$true)]
            [psobject]$infra 
    )
    $result = @()
    $objects = $infra.children | Where-Object { $_.resource_type -eq "ChildPolicyContextProfile" }
    if ($objects) {
        $result += ($objects | Where-Object { $_.PolicyContextProfile._system_owned -ne $true })
    }
    $objects = $infra.children | Where-Object { $_.resource_type -eq "ChildService" }
    if ($objects) {
        ### Also, make sure that "children" property is empty
        $services = $objects | Where-Object { $_.Service._system_owned -ne $true }
        $serviceChildren = @()
        foreach($service in $services){
            $service.Service.children = $serviceChildren
            $result += $service
        }
    }
    $domains = $infra.children | Where-Object { $_.resource_type -eq "ChildDomain" }
    foreach ($domain in $domains) {
        $newDomainChildren = @()   
        ## groups
        $objects = $domain.Domain.children | Where-Object { $_.resource_type -eq "ChildGroup" }
        if ($objects) {
            $newDomainChildren += ($objects | Where-Object { $_.Group._system_owned -ne $true })
        }
        ## security policies
        $securityPolicies = $domain.Domain.children | Where-Object { $_.resource_type -eq "ChildSecurityPolicy" }
        foreach ($securityPolicy in $securityPolicies) {
            $newPolicyChildren = @() 
            $objects = $securityPolicy.SecurityPolicy.children | Where-Object { $_.resource_type -eq "ChildRule" }
            if ($objects) {
                $newPolicyChildren += ($objects | Where-Object { $_.Rule._system_owned -ne $true })
            }
            $securityPolicy.SecurityPolicy.children = $newPolicyChildren
            $newDomainChildren += $securityPolicy
        }
        $domain.Domain.children = $newDomainChildren
        $result += $domain 
    }
    $infra.children = $result
    $infra
}

function Remove-UnsupportedGroupMembership {
    param (
        [Parameter(Mandatory=$true)]
            [psobject]$infra 
    )
    $result = @()
    $result += $infra.children | Where-Object { $_.resource_type -eq "ChildPolicyContextProfile" }
    $result += $infra.children | Where-Object { $_.resource_type -eq "ChildService" }

    $domains = $infra.children | Where-Object { $_.resource_type -eq "ChildDomain" }
    foreach ($domain in $domains) {
        $newDomainChildren = @()   
        ## groups
        $groups = $domain.Domain.children | Where-Object { $_.resource_type -eq "ChildGroup" }
        #### code for omitting group altogether
        # foreach ($group in $groups){
        #     $a = 0
        #     $b = 0
        #     $a = ($group.Group.expression | Where-Object { $_.resource_type -eq "ExternalIDExpression"}).count
        #     $b = ($group.Group.expression | Where-Object { $_.resource_type -eq "PathExpression"}).count
        #     if ($a -or $b) {
        #         Write-Host "Group $($group.Group.display_name) contains unsupported static membership criteria - omitting"
        #     }
        #     else {
        #         $newDomainChildren += $group
        #     }
        # }
        foreach ($group in $groups) {
            $newExpression = @()
            foreach ($expression in $group.Group.expression) {
                if (($expression.resource_type -eq "ExternalIDExpression") -or ($expression.resource_type -eq "PathExpression")) {
                    Write-Host "Group $($group.Group.display_name) contains unsupported membership criteria. Those criteria will be omitted"
                }
                else {
                    $newExpression += $expression
                }
            }
            $group.Group.expression = $newExpression
            $newDomainChildren += $group
        }
        ## security policies
        $newDomainChildren += $domain.Domain.children | Where-Object { $_.resource_type -eq "ChildSecurityPolicy" }
        $domain.Domain.children = $newDomainChildren
        $result += $domain 
    }
    $infra.children = $result
    $infra
}

function Get-ObjectsNumber {
    param (
        [Parameter(Mandatory=$true)]
            [psobject]$infra 
    )
    $result = 0
    $result += $infra.children.Count
    $domains = $infra.children | Where-Object { $_.resource_type -eq "ChildDomain" }
    foreach ($domain in $domains) {
        $result += $domain.Domain.children.Count
        $securityPolicies = $domain.Domain.children | Where-Object { $_.resource_type -eq "ChildSecurityPolicy" }
        foreach ($securityPolicy in $securityPolicies) {
            $result += $securityPolicy.SecurityPolicy.children.count
        }
    }
    $result
}

# % - ForEach-Object
# function clone($obj)
# {
#     $newobj = New-Object PsObject
#     $obj.psobject.Properties | % {Add-Member -MemberType NoteProperty -InputObject $newobj -Name $_.Name -Value $_.Value}
#     return $newobj
# }

function Copy-Object {
    [CmdletBinding()] Param (
        [Parameter(ValueFromPipeline)] [object[]]$objects,
        [Parameter()] [int] $depth = 100
    )
    $clones = foreach ($object in $objects) {
        $object | ConvertTo-Json -Compress -depth $depth | ConvertFrom-Json
    }
    return $clones
}

### Let's find objects that exist on Destination, but not on Source - we'll need to delete those objects
function Get-ObjectsToDelete {
    param (
        [Parameter(Mandatory=$true)]
            [PSCustomObject]$Source,
            [Parameter(Mandatory=$true)]
            [PSCustomObject]$Destination
    )
    $newChildren = @()
    foreach ($object in $Destination.children) {
        $checkObj = $null
        if ($object.resource_type -eq "ChildPolicyContextProfile") {
            $checkObj = $Source.children | Where-Object { $_.resource_type -eq "ChildPolicyContextProfile"} | Where-Object { $_.PolicyContextProfile.id -eq $object.PolicyContextProfile.id }
            if (!$checkObj) {
                $temp = Copy-Object -objects $object
                $temp.marked_for_delete = $true
                $newChildren += $temp
            }
        }
        if ($object.resource_type -eq "ChildService") {
            $checkObj = $Source.children | Where-Object { $_.resource_type -eq "ChildService"} | Where-Object { $_.Service.id -eq $object.Service.id }
            if (!$checkObj) {
                $temp = Copy-Object -objects $object
                $temp.marked_for_delete = $true
                $newChildren += $temp
            }
        }
        if ($object.resource_type -eq "ChildDomain") {
            $tempDomain = Copy-Object -objects $object
         
            $newDomainChildren = @()
            $sourceDomain = $Source.children | Where-Object { $_.resource_type -eq "ChildDomain"} | Where-Object { $_.Domain.id -eq $object.Domain.id }
            
            if ($sourceDomain) {
                $tempDomain.Domain.children = @()
                foreach ($domainChildObject in $object.Domain.children) {
                    if ($domainChildObject.resource_type -eq "ChildGroup") {
                        $checkObj = $sourceDomain.Domain.children | Where-Object { $_.resource_type -eq "ChildGroup" } | Where-Object { $_.Group.id -eq $domainChildObject.Group.id }
                        if (!$checkObj) {
                            $temp = Copy-Object -objects $domainChildObject
                            $temp.marked_for_delete = $true
                            $newDomainChildren += $temp
                        }                        
                    }
                    if ($domainChildObject.resource_type -eq "ChildSecurityPolicy") {
                        $tempSecurityPolicy = Copy-Object -objects $domainChildObject
                        $sourceSecurityPolicy = $sourceDomain.Domain.children | Where-Object { $_.resource_type -eq "ChildSecurityPolicy" } | Where-Object { $_.SecurityPolicy.id -eq $domainChildObject.SecurityPolicy.id }
                        ### if ChildSecurityPolicy exists on the source - iterate it and check for the objects to delete
                        if ($sourceSecurityPolicy) {
                            $tempSecurityPolicy.SecurityPolicy.children = @()
                            $newSecurityPolicyChildren = @()
                            foreach ($policyChildObject in $domainChildObject.SecurityPolicy.children) {
                                if ($policyChildObject.resource_type -eq "ChildRule") {
                                    $checkObj = $sourceSecurityPolicy.SecurityPolicy.children | Where-Object { $_.resource_type -eq "ChildRule"} | Where-Object { $_.Rule.id -eq $policyChildObject.Rule.id }
                                    if (!$checkObj) {
                                        $temp = Copy-Object -objects $policyChildObject
                                        $temp.marked_for_delete = $true
                                        $newSecurityPolicyChildren += $temp
                                    }
                                }
                            }
                            $tempSecurityPolicy.SecurityPolicy.children = $newSecurityPolicyChildren
                        }
                        ### if ChildSecurityPolicy doesn't exist in the source configuration, mark it for deletion
                        else {
                            $tempSecurityPolicy.marked_for_delete = $true
                        }
                        $newDomainChildren += $tempSecurityPolicy
                    }
                }
                $tempDomain.Domain.children = $newDomainChildren
            }
            else { ### in the unlikely event some custom domain was created and the removed - mark it for deletion
                $tempDomain.marked_for_delete = $true
            }
            $newChildren += $tempDomain
        }
    }
    $result = Copy-Object -objects $Destination
    $result.children = $newChildren
    $result
}

### Script body start here

if (!$UserIN) {
	$UserIN = "admin"
}
if (!$UserOUT) {
	$UserOUT = "admin"
}
if (!$PasswordIN) {
	$securePwd = Read-Host "Source System Password: " -AsSecureString
# Converting from Secure string back to Plain	
	$BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($securePwd)
	$PasswordIN = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR)
}
if (!$PasswordOUT) {
	$securePwd = Read-Host "Destination System Password: " -AsSecureString
# Converting from Secure string back to Plain	
	$BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($securePwd)
	$PasswordOUT = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR)
}

try {
    Write-Host "Step 1. Gathering data from the source system..."
	$NSXConnectionIN = Connect-NSXTManager -Server $NSXManagerIN -Username $UserIN -Pwd $PasswordIN
}
catch {
	throw "Cannot connect to the source NSX-T. $_"   
}

try {
    $invINRulesOrig = Invoke-NSXRestAPICall -Connection $NSXConnectionIN -URI "/policy/api/v1/infra?filter=Type-Domain|SecurityPolicy|Rule" -Method "get"
    $invINSupportOrig = Invoke-NSXRestAPICall -Connection $NSXConnectionIN -URI "/policy/api/v1/infra?filter=Type-Domain|Group|PolicyContextProfile|Service" -Method "get" #add service after all the debug
}
catch {
	throw "Cannot get data from the source NSX-T. $_"   
}

### Filter out System-owned objects from Source config
try {
    $invINRules = Remove-SystemOwnedObjects -infra $invINRulesOrig
    $invINSupport = Remove-SystemOwnedObjects -infra $invINSupportOrig
}
catch {
    throw "Cannot filter out System-owned objects for source NSX-T. $_"
}

### Filter out unsupported membership criteria from groups
try {
    $invINSupport = Remove-UnsupportedGroupMembership -infra $invINSupport
}
catch {
    throw "Cannot filter out unsupported membership criteria from groups on the source NSX-T. $_"
}

### Get config from the destination system
try {
    Write-Host "Step 2. Gathering data from the destination system..."
	$NSXConnectionOUT = Connect-NSXTManager -Server $NSXManagerOUT -Username $UserOUT -Pwd $PasswordOUT
}
catch {
	throw "Cannot connect to the destination NSX-T. $_"   
}

try {
    $invOUTRulesOrig = Invoke-NSXRestAPICall -Connection $NSXConnectionOUT -URI "/policy/api/v1/infra?filter=Type-Domain|SecurityPolicy|Rule" -Method "get"
    $invOUTSupportOrig = Invoke-NSXRestAPICall -Connection $NSXConnectionOUT -URI "/policy/api/v1/infra?filter=Type-Domain|Group|PolicyContextProfile|Service" -Method "get" #add service after all the debug
}
catch {
	throw "Cannot query data from the destination NSX-T. $_"   
}

### Filter out System-owned objects from Destination config
try {
    $invOUTRules = Remove-SystemOwnedObjects -infra $invOUTRulesOrig
    $invOUTSupport = Remove-SystemOwnedObjects -infra $invOUTSupportOrig
}
catch {
    throw "Cannot filter out System-owned objects for destination NSX-T. $_"
}

### Let's find objects that exist on Destination, but not on Source - we'll need to delete those objects
$invDELSupport = Get-ObjectsToDelete -Source $invINSupport -Destination $invOUTSupport
$invDELRules = Get-ObjectsToDelete -Source $invINRules -Destination $invOUTRules
# $str = $invDELSupport | ConvertTo-Json -Depth 15
# $str.Replace('"marked_for_delete": true','"marked_for_delete": false')
# $str

### More than 2000 objects in a single API call will throw an error. Might as well stop here
Write-Host "Step 3. Checking number of Policy objects..."
$a = 0
$b = 0
$c = 0
$d = 0
$a = Get-ObjectsNumber -infra $invINRules
$b = Get-ObjectsNumber -infra $invINSupport
$c = Get-ObjectsNumber -infra $invDELRules
$d = Get-ObjectsNumber -infra $invDELSupport
if ((($a + $c) -ge 2000) -or ($b -ge 2000) -or ($d -ge 2000)) {
    throw "There would be more than 2000 objects in a single Policy API Hierarchy call. That is not supported"
}

### create support objects on a destination system
try {
    Write-Host "Step 4. Creating support objects on the destination system..."
    Invoke-NSXRestAPICall -Connection $NSXConnectionOUT -URI "/policy/api/v1/infra/" -Method "patch" -JSONBody ($invINSupport | ConvertTo-Json -Depth 15) | Out-Null
}
catch {
    throw "Cannot create Security Groups and Policy Context profiles on a destination system. $_"
}

### Run delete/create Policies/Rules call on a destination system
try {
    Write-Host "Step 5. Updating ruleset on the destination system..."
    $JSONBody = Copy-Object -objects $invINRules
    $JSONBody.children += $invDELRules.children
    Invoke-NSXRestAPICall -Connection $NSXConnectionOUT -URI "/policy/api/v1/infra/" -Method "patch" -JSONBody ($JSONBody | ConvertTo-Json -Depth 15) | Out-Null
}
catch {
    #### implement rollback
    throw "There was an error updating ruleset on a destination system. $_"
}

### Delete unneeded support objects on a destination system
try {
    Write-Host "Step 6. Removing unneeded support objects from the destination system..."
    Invoke-NSXRestAPICall -Connection $NSXConnectionOUT -URI "/policy/api/v1/infra/" -Method "patch" -JSONBody ($invDELSupport | ConvertTo-Json -Depth 15) | Out-Null
}
catch {
    throw "There was an error deleting unneeded support objects from a destination system. They must be deleted manually. $_"
}