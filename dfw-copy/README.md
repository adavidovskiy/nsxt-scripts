Tool to replicate DFW configuration from the source to the destination NSX-T instance

Policy API objects only!
Supported object types: Domain, Group, SecurityPolicy, Rule, PolicyContextProfile, Service
Ruleset:
* SecurityPolicy
* Rule
Support objects:
* Group
* Service
* PolicyContextProfile

Limitations:
* Non-IP based static Security Group membership is not supported (defined by external_id field)
    * Segment, SegmentPort and VirtualNetworkInterface objects are not supported.
* 2000 objects maximum for backup/restore functionality. Hierarchial Infra API call limitation.

Order of operations:
* Step 1. Gather data from the source system
    * Filter out system-owned objects
    * Check for unsupported group membership criteria - those membership criteria are removed
* Step 2. Gather data from the destination system
    * Filter out system-owned objects
    * Find unneeded objects - the ones that exist on destination but not on source
* Step 3. Calculate maximum number of objects per Policy API call
* Step 4. Create support objects on the destination system
* Step 5. Update ruleset on the destination system
* Step 6. Remove unneeded support objects from the destination system

Script parameters:
* NSXManagerIN - FQDN or IP of the source NSX-T Manager
* UserIN - Source system user. Tested only with "admin". Defaults to "admin" if nothing is provided
* PasswordIN - Source system password
* NSXManagerOUT - FQDN or IP of the destination NSX-T Manager
* UserOUT - Destination system user. Tested only with "admin". Defaults to "admin" if nothing is provided
* PasswordOUT - Destination system password